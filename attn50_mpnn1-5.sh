#!/usr/bin/env bash
#SBATCH --job-name=mpnn20-501-4
#SBATCH --output=mpnn20-501-4%j.log
#SBATCH --error=mpnn20-501-4%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

#WANDB_MODE=dryrun
wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python attn50_mpnn1-5.py 

CUDA_VISIBLE_DEVICES="$DEVICES" python attn50_mpnn2.py

CUDA_VISIBLE_DEVICES="$DEVICES" python attn50_mpnn4.py



















